# Montando uma rádio web com Software Livre.
  Este é um simples script para automatizar o processo de instalação de uma rádio web com software livre. Todo processo padrão está documentado neste [link](https://blog.ufba.br/radiofaced/?page_id=311)
   O objetivo de sua criação foi simplificar todo o processo, visto que nem todos os usuários tem a facilidade para executar o passo a passo descrito no [link](https://blog.ufba.br/radiofaced/?page_id=311).

  Configurações básicas para criação da rádio web:

  * Computador, com distribuição [Linux](https://pt.wikipedia.org/wiki/Distribui%C3%A7%C3%A3o_Linux) instalado (Atualmente este script somente irá funcionar para distribuições baseadas no [Debian](https://pt.wikipedia.org/wiki/Debian) ). 
    **Nos teste realizadados foram utilizados as distribuições: Lubuntu 17, Ubuntu 15, Debian 7**
  * Servidor [Icecast](http://icecast.org/).
 

### Download Radio Web

 **Existem três modos de realizar o download do pacote:**
- 1. Navegador
- 2. Terminal
    - 2.1 wget
    - 2.2 git
        
##### 1. Navegador

1. Faça Download
2. [Clique](https://gitlab.com/luancmenezes/radioweb/repository/archive.zip?ref=master)


#### 2. Terminal

###### 2.1 wget
    
1. Abra o terminal
2. Digite o comando e depois ENTER:

  ```shell
  $ wget https://gitlab.com/luancmenezes/radioweb/repository/archive.zip?ref=master
  ```

###### 2.2 git
1. Abra o terminal
2. Digite o comando (instalação git): 

```shell
  $ sudo apt-get install git
  ```
  
3. Digite o comando (Download repositório git):

  ```shell
  $ git clone https://gitlab.com/luancmenezes/radioweb.git
  ``` 
 
 
### Instalação da Radio Web
  Após o download abra o terminal como root:
  
  ```shell
  $ sudo su
  ```
  Acesse a pasta onde está localizado o arquivo que fez o download.
  
  ```shell
  # cd caminho/pasta
  ```
  Dentro da pasta execute o comando ```shell chmod +x ``` para tornarmos o arquivo *radioweb.sh* como executável.

  ```shell
  # chmod +x radioweb.sh
  ```
  Agora digite o comando abaixo para inciar a parte de gráfica.

  ```shell
  # ./radioweb.sh
  ```
**Desenvoldido por Luan Menezes - luancmenezes@gmail.com**
**Ultima atualização: 28/07/2017**
