#!/bin/bash
# Criado em:  10/Fev/2016
# Autor: Luan Menezes - luancmenezes@gmail.com
#

DIR1=/home/radio
DIR2=/usr/share/doc/ices2/examples/ices-alsa.xml
DIR3=/home/radio/ices-alsa.xml
DIR4=/etc/xdg/autostart



while true; do
  choice="$(zenity --width=500 --height=600 --list --column "Rádio Web" --title="Rádio Web" \
  "Tutorial" \
  "Instalar/Reconfigurar" \
  "Iniciar" \
  "Rádio automática" \
  "Sair")"


  case "${choice}" in
		"Instalar/Reconfigurar" )
  	   mkdir /home/radio
  	   apt-get install ices2

      name_radio=`zenity --width=350 --height=100 --entry --title "Nome da rádio" `
     	ponto_montagem=`zenity --width=350 --height=100 --entry --title "Ponto de montagem" `
  		ponto_montagem="$(echo $ponto_montagem | sed 's/ /_/g')"
   		name_genre=`zenity --width=350 --height=100 --entry --title "Gênero da rádio" `
  		name_description=`zenity --width=350 --height=100 --entry --title "Descrição da rádio" `
      website=`zenity --width=350 --height=100 --width=320 --height=100 --entry --title "URL" `
  		hostname=`zenity --width=350 --height=100 --entry --title "Servidor" `
  		password=`zenity --width=350 --height=100 --entry --title "Senha" `
      #device = `zenity --width=320 --height=100 --entry --title "Insira o número do Dispositivo" --entry-text "Ex: 0,0"`
      #canais = `zenity --width=320 --height=100 --entry --title "INSIRA INFORMAÇÕES DA RADIO WEB" --entry-text "Quantidade de Canais"`

		ICESALSA='<?xml version="1.0"?>
<ices>

    <!-- run in background  -->
    <background>0</background>
    <!-- where logs go. -->
    <logpath>/var/log/ices</logpath>
    <logfile>ices.log</logfile>
    <!-- size in kilobytes -->
    <logsize>2048</logsize>
    <!-- 1=error, 2=warn, 3=infoa ,4=debug -->
    <loglevel>4</loglevel>
    <!-- logfile is ignored if this is set to 1 -->
    <consolelog>1</consolelog>

    <!-- optional filename to write process id to -->
    <!-- <pidfile>/home/ices/ices.pid</pidfile> -->

    <stream>
        <!-- metadata used for stream listing -->
        <metadata>
            <name>'$name_radio'</name>
            <genre>'$name_genre'</genre>
            <description>'$name_description'</description>
            <url>http://'$website'</url>
        </metadata>

        <!--    Input module.

            This example uses the "alsa" module. It takes input from the
            ALSA audio device (e.g. line-in), and processes it for live
            encoding.  -->
        <input>
            <module>alsa</module>
            <param name="rate">48000</param>
            <param name="channels">2</param>
            <param name="device">hw:0,0</param>
            <!-- Read metadata (from stdin by default, or -->
            <!-- filename defined below (if the latter, only on SIGUSR1) -->
            <param name="metadata">1</param>
            <param name="metadatafilename">test</param>
        </input>

        <!--    Stream instance.

            You may have one or more instances here.  This allows you to
            send the same input data to one or more servers (or to different
            mountpoints on the same server). Each of them can have different
            parameters. This is primarily useful for a) relaying to multiple
            independent servers, and b) encoding/reencoding to multiple
            bitrates.

            If one instance fails (for example, the associated server goes
            down, etc), the others will continue to function correctly.
            This example defines a single instance doing live encoding at
            low bitrate.  -->

        <instance>
            <!--    Server details.

                You define hostname and port for the server here, along
                with the source password and mountpoint.  -->

            <hostname>'$hostname'</hostname>
            <port>8000</port>
            <password>'$password'</password>
            <mount>/'$ponto_montagem'.ogg</mount>
            <yp>1</yp>   <!-- allow stream to be advertised on YP, default 0 -->

            <!--    Live encoding/reencoding:

                channels and samplerate currently MUST match the channels
                and samplerate given in the parameters to the alsa input
                module above or the remsaple/downmix section below.  -->

            <encode>
                <quality>0</quality>
                <samplerate>44100</samplerate>
                <channels>1</channels>
            </encode>

            <!-- stereo->mono downmixing, enabled by setting this to 1 -->
            <downmix>1</downmix>

            <!-- resampling.

                Set to the frequency (in Hz) you wish to resample to, -->

            <resample>
                <in-rate>44100</in-rate>
                <out-rate>44100</out-rate>
            </resample>

             <savefile>./gravacao_transmissao01.ogg</savefile>

        </instance>

    </stream>
</ices>'


		transmissao_radio=`dirname $0`/file/transmissao_radio.sh
		cp $transmissao_radio $DIR1
		echo "${ICESALSA}" > $DIR3


		chmod +x /home/radio/transmissao_radio.sh

		cd /home/radio
		./transmissao_radio.sh

;;
		"Iniciar")
		cd /home/radio
		./transmissao_radio.sh

		 ;;
		"Rádio automática")

			choice="$(zenity --width=270 --height=240 --list --column "Rádio Web" --title="Rádio Web" \
			  "Debian" \
			  "Ubuntu" \
			  "Lubuntu" \
			  "Sair")"

			   case "${choice}" in

		"Debian")

		script=`dirname $0`/file/sedTransmissao_auto.sh
		chmod +x $script
		./$script
		zenity --title="Caixa Info" --info --text="Radio automatica finalizada, reinicie o computador. TERMINAL #reboot"
		;;

		"Ubuntu")

		script=`dirname $0`/file/ubuntu_auto.sh
		chmod +x $script
		./$script
		zenity --title="Caixa Info" --info --text="Radio automatica finalizada, reinicie o computador. TERMINAL #reboot"
		;;

		"Lubuntu")

		radio=`dirname $0`/file/radioauto.desktop
		cp $radio $DIR4

		zenity --title="Caixa Info" --info --text="Radio automatica finalizada, reinicie o computador. TERMINAL #reboot"
		;;

		esac

		;;
       "Tutorial")

		FILE=`dirname $0`/README

		zenity --text-info --width=500 --height=600 \
		--title="Tutorial" \
		--filename=$FILE \

		;;
        *)
            break
          ;;
        esac

done
